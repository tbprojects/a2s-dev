import { Component, Input } from '@angular/core';
import { SpaceShip } from '../space-ship';
import { ZoomImageDirective } from '../../shared/zoom-image.directive';
import { TickizePipe } from '../../shared/tickize.pipe';

@Component({
  selector: 'app-space-ship',
  imports: [ZoomImageDirective, TickizePipe],
  templateUrl: './space-ship.component.html',
  styleUrl: './space-ship.component.css'
})
export class SpaceShipComponent {
  @Input({required: true}) spaceShip!: SpaceShip;

}
