import { TestBed } from '@angular/core/testing';

import { destructionGuard } from './destruction.guard';
import { CanActivateFn } from '@angular/router';

describe('destructionGuard', () => {
  const executeGuard: CanActivateFn = (...guardParameters) =>
    TestBed.runInInjectionContext(() => destructionGuard(...guardParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeGuard).toBeTruthy();
  });
});
