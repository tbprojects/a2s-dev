import { Component } from '@angular/core';
import { DecoderResult } from '../decoder-result';
import { DecoderService } from '../decoder.service';
import { map, scan, startWith } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { DecoderFeedbackItemComponent } from '../decoder-feedback-item/decoder-feedback-item.component';
import { AsyncPipe } from '@angular/common';

@Component({
  selector: 'app-decoder-feedback-list',
  templateUrl: './decoder-feedback-list.component.html',
  imports: [
    DecoderFeedbackItemComponent,
    AsyncPipe
  ],
  styleUrls: ['./decoder-feedback-list.component.css']
})
export class DecoderFeedbackListComponent {
  decoderResults: Observable<DecoderResult[]> = this.decoder.decoderResults
    .pipe(
      scan<DecoderResult, DecoderResult[]>((acc, current) => [current, ...acc], []),
      startWith([])
    );
  decoderResultsCount: Observable<number> = this.decoderResults
    .pipe(
      map(results => results.length)
    );

  constructor(private decoder: DecoderService) { }
}
