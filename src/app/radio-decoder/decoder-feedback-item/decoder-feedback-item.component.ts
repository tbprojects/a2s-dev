import { Component, Input } from '@angular/core';
import { DecoderResult } from '../decoder-result';
import { DecoderLightComponent } from '../decoder-light/decoder-light.component';
import { NumberToArrayPipe } from '../number-to-array.pipe';

@Component({
  selector: 'app-decoder-feedback-item',
  templateUrl: './decoder-feedback-item.component.html',
  imports: [
    DecoderLightComponent,
    NumberToArrayPipe
  ],
  styleUrls: ['./decoder-feedback-item.component.css']
})
export class DecoderFeedbackItemComponent {
  @Input() result!: DecoderResult;
  @Input() index!: number;
}
