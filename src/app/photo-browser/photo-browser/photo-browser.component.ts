import { Component, OnInit } from '@angular/core';
import { PhotoFormComponent } from '../photo-form/photo-form.component';
import { PhotoListComponent } from '../photo-list/photo-list.component';

@Component({
  selector: 'app-photo-browser',
  templateUrl: './photo-browser.component.html',
  imports: [
    PhotoFormComponent,
    PhotoListComponent
  ],
  styleUrls: ['./photo-browser.component.css']
})
export class PhotoBrowserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
