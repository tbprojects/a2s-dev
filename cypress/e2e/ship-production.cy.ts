describe('ShipProduction', () => {
  it('produces ships', () => {
    cy.visit('/');
    cy.get('app-engineers-room').within(() => {
      cy.get('input[type=radio]').first().click();
      cy.get('input[type=number]').clear().type('2');
      cy.get('form button').click();
    })
    cy.wait(4000);
    cy.get('app-space-ship').should('have.length', 2);
  });
});
